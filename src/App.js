import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
       <h1>Movie Search</h1>
        <div className="container">
          <div class="container">
          <input placeholder="search film...." class="block mx-auto my-4 rounded outline-2 font-mono px-2 py-2 text-gray-400 text-md focus:outline-blue-500 "/>
          </div>
            <div class="container mx-auto bg-white rounded w-2/5 ">
              <div class="text-gray-900 font-mono ">Example</div>  
              <img class="w-full"  src={logo}/> 
              <div class="text-gray-900 text-sm">12-12-12</div>
              <div class="text-white-900 text-base bg-regal-blue">9.9</div>
            </div>
        </div>
      </header>
    </div>
  );
}

export default App;
